#************************************************
# Author: Brandon Mendrick
# This file will compute the step for reducing
# a number down to 1 using the Collatz Conjecture
#************************************************

import sys

# Function to handle even numbers 
# In this case, the number is divided by 2
def even(num):
	tmp = []
	while (num % 2 == 0):
		num = num / 2
		tmp.append(num)
	return tmp

# Function to handle odd numbers
# In this case, the number is multiplied by 3 and 1 is added to it
# Since we know this results in an even number, this computation is
# performed here as well.
def odd(num):
	tmp = []
	while (num % 2 != 0 and num != 1):
		num = (3 * num) + 1
		tmp.append(num)
		num = num / 2
		tmp.append(num)
	return tmp

# Function to determine if the most recent number is even or odd.
# After the determination, it calls the corresponding function
# and adds the results into the path list variable.
def reduce(num):
	path = [num]

	# loop until the last entry in the path list is 1
	while path[-1] != 1:
		if path[-1] % 2 == 0:
			tmp = even(path[-1])
		else:
			tmp = odd(path[-1])
		
		# adding the results to the path list
		path.extend(tmp)
	
	# returning the result
	return path

# function to handle user input and console output
def main():
	num = sys.argv[1]	# user input number

	path = reduce(int(num))
	print(path)
	print "# of steps: " + str(len(path))

# handling command main calling
if __name__ == '__main__':
	main()
